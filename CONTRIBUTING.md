# Contributing

Craplog is under development and there's often something to do.

Get a look at the [issues](https://git.disroot.org/elB4RTO/craplog-CLI/issues) page to get rid of what's going on.

<br/>

## Who can contribute

Everyone is welcome,

<br/>

## How to contribute

If you don't know how to contribute to a project, follow these steps:
  - **Fork** this project (will appear in your own repositories)
  - **Improve** it with your ideas
  - Submit a **Pull Request**

<br/>

## Pull Requests guideline

When submitting a PR, please leave a message with it explaining which has been your changes,

<br/>
